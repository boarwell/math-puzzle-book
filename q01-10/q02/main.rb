op = ['*', '']
nums = []

1000.upto(9999) do |i|
  c = i.to_s
  next if c.include?('0')
  0.upto(op.size - 1) do |j|
    0.upto(op.size - 1) do |k|
      0.upto(op.size - 1) do |l|
        expression = c[0] + op[j] + c[1] + op[k] + c[2] + op[l] + c[3]
        if expression.length > 4
          ans = eval(expression)
          nums.push c if ans.to_s == c.reverse
        end
      end
    end
  end
end

puts nums