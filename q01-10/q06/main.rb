def collatz_kai(n, m)
  return 0 if m == 1
  return 1 if m == n

  # 再帰呼び出しのどちらか片方にはreturnをつけないと
  # 下のループでnil can't be coerced into Integer (TypeError)というエラーが出る
  # 戻り値なしの関数とみなされてしまうんでしょうか
  return collatz_kai(n, m / 2) if m.even?
  collatz_kai(n, m * 3 + 1)
end

count = 0
2.upto(10_000) do |i|
  count += collatz_kai(i, i * 3 + 1)
end

puts count
