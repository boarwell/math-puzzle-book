n = 10
@steps = 4
@memo = {}

def move(a, b)
  return @memo[[a,b]] if @memo.key? [a,b]
  return 0 if a > b
  return 1 if a == b

  count = 0
  1.upto(@steps) do |da|
    1.upto(@steps) do |db|
      count += move(a + da, b - db)
    end
  end

  @memo[[a,b]] = count
end

puts move(0, n)
