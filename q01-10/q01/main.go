package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	n := 11
	for {
		if isPalindrome(strconv.Itoa(n)) &&
			isPalindrome(strconv.FormatInt(int64(n), 2)) &&
			isPalindrome(strconv.FormatInt(int64(n), 8)) {
			break
		}
		n += 2
	}
	fmt.Println(n)
}

func isPalindrome(n string) bool {
	if n == palindrome(n) {
		return true
	}
	return false
}

func palindrome(n string) string {
	s := strings.Split(n, "")
	b := bytes.NewBufferString("")

	for i := len(s) - 1; i > -1; i-- {
		b.WriteString(s[i])
	}

	return b.String()
}
