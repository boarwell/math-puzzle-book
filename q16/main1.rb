max = 500

answer = []

(1..max / 4).each do |c|
  # v -> 長方形の縦の長さ
  # 2 * c - v -> 長方形の横の長さ
  edge = (1..(c - 1)).to_a.map { |v| v * (2 * c - v) }
  edge.combination(2) do |a, b|
    answer.push([1, b / a.to_f, c * c / a.to_f]) if a + b == c * c
  end
end

answer.uniq!
puts answer.size
