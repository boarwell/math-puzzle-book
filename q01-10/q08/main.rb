@n = 12

def move(log)
  return 1 if log.size == @n + 1

  count = 0
  [[0, 1], [0, -1], [1, 0], [-1, 0]].each do |d|
    next_pos = [log[-1][0] + d[0], log[-1][1] + d[1]]
    count += move(log + [next_pos]) unless log.include?(next_pos)
  end

  count
end

puts move([[0, 0]])
